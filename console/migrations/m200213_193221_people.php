<?php

use yii\db\Migration;

/**
 * Class m200213_193221_people
 */
class m200213_193221_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200213_193221_people cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->createTable(
            'people',
            [
                'id' => $this->primaryKey(),
                'iin_bin' => $this->string(20)->comment('ИИН'),
                'full_name' => $this->string(300)->comment('ИфО'),
                'total_arrears' => $this->string(300)->comment('общая задолженность'),
                'pension_contribution_arrears' => $this->string(300)->comment('Задолженность по пенсионному взносу'),
                'social_contribution_arrears' => $this->string(300)->comment('Задолженность по социальному взносу'),
                'social_healthInsurance_arrears' => $this->string(300)->comment('Задолженность по социальному страхованию здоровья'),
                'appealed_amount' => $this->string(300)->comment('Апелляционная сумма'),
                'modified_terms_amount' => $this->string(300)->comment('измененные условия Сумма'),
                'rehabilitation_procedure_amount' => $this->string(300)->comment('Процедура реабилитации Сумма'),
                'send_time' => $this->string(300)->comment('Время отправки'),
                'tax_org_info' => $this->text()->comment('налоговая организация'),
                'created_at' => $this->string(300)->comment('когда добавлялся информация'),
                'updated_at' => $this->string(300)->comment('когда обновлялась информация'),
                'is_status' => $this->tinyInteger(2),
            ]
        );
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropTable('people');
    }
}
