<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "people".
 *
 * @property int $id
 * @property string|null $iin_bin ИИН
 * @property string|null $full_name ИфО
 * @property string|null $total_arrears общая задолженность
 * @property string|null $pension_contribution_arrears Задолженность по пенсионному взносу
 * @property string|null $social_contribution_arrears Задолженность по социальному взносу
 * @property string|null $social_healthInsurance_arrears Задолженность по социальному страхованию здоровья
 * @property string|null $appealed_amount Апелляционная сумма
 * @property string|null $modified_terms_amount измененные условия Сумма
 * @property string|null $rehabilitation_procedure_amount Процедура реабилитации Сумма
 * @property string|null $send_time Время отправки
 * @property string|null $tax_org_info налоговая организация
 * @property string|null $created_at когда добавлялся информация
 * @property string|null $updated_at когда обновлялась информация
 * @property int|null $is_status
 */
class People extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'people';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tax_org_info'], 'string'],
            [['is_status'], 'default', 'value' => null],
            [['is_status'], 'integer'],
            [['iin_bin'], 'string', 'max' => 20],
            [['full_name', 'total_arrears', 'pension_contribution_arrears', 'social_contribution_arrears', 'social_healthInsurance_arrears', 'appealed_amount', 'modified_terms_amount', 'rehabilitation_procedure_amount', 'send_time', 'created_at', 'updated_at'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'iin_bin' => Yii::t('app', 'Iin Bin'),
            'full_name' => Yii::t('app', 'Full Name'),
            'total_arrears' => Yii::t('app', 'Total Arrears'),
            'pension_contribution_arrears' => Yii::t('app', 'Pension Contribution Arrears'),
            'social_contribution_arrears' => Yii::t('app', 'Social Contribution Arrears'),
            'social_healthInsurance_arrears' => Yii::t('app', 'Social Health Insurance Arrears'),
            'appealed_amount' => Yii::t('app', 'Appealed Amount'),
            'modified_terms_amount' => Yii::t('app', 'Modified Terms Amount'),
            'rehabilitation_procedure_amount' => Yii::t('app', 'Rehabilitation Procedure Amount'),
            'send_time' => Yii::t('app', 'Send Time'),
            'tax_org_info' => Yii::t('app', 'Tax Org Info'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_status' => Yii::t('app', 'Is Status'),
        ];
    }
}
