<?php

namespace frontend\controllers\service;

use Sabirov\AntiCaptcha\ImageToText;

class ParsersService extends \yii\base\Component
{
    /**
     * @param string $url
     * @return bool
     */
    public function receiveNameCaptcha(string $url)
    {
        $anticaptcha = new ImageToText();
        $anticaptcha->setVerboseMode(true);
        $anticaptcha->setKey('7ab1a26549ec3798e80e61e3feaeed7e');
        $anticaptcha->setFile($url);

        if (!$anticaptcha->createTask()) {
            $anticaptcha->debout("API v2 send failed - " . $anticaptcha->getErrorMessage(), "red");

            return false;
        }

        $taskId = $anticaptcha->getTaskId();

        if (!$anticaptcha->waitForResult()) {
            $anticaptcha->debout("could not solve captcha", "red");
            $anticaptcha->debout($anticaptcha->getErrorMessage());
        }

        return $anticaptcha->getTaskResult();
    }

    /**
     * @param string $imageUrl
     * @param string $pathPreservation
     * @return string
     */
    public function downloadCaptcha(string $imageUrl, string $pathPreservation): string
    {
        $ch = curl_init($imageUrl);
        $fp = fopen($pathPreservation, 'wb');

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);

        fclose($fp);

        return $pathPreservation;
    }
}
