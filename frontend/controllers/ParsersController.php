<?php

namespace frontend\controllers;

use common\models\People;
use frontend\controllers\service\ParsersService;
use GuzzleHttp\Client;
use Unirest\Request;
use yii\base\Model;
use yii\web\Controller;

set_time_limit(1000);

class ParsersController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $parsesService = new ParsersService();
        $postRequest = \Yii::$app->request->post();
        $person = [];

        if (!empty($postRequest['iin'])) {
            $headers =
                [
                    'Connection' => 'keep-alive',
                    'Accept' => 'application/json',
                    'DNT' => '1',
                    'X-Requested-With' => 'XMLHttpRequest',
                    'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                    'Content-Type' => 'application/json',
                    'Origin' => 'http://kgd.gov.kz',
                    'Referer' => 'http://kgd.gov.kz/ru/app/culs-taxarrear-search-web',
                    'Accept-Encoding' => 'gzip, deflate',
                    'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Cookie' => '_ga=GA1.3.1305856964.1581588041; has_js=1; kgd_spec_version=normal; kgd_spec_color=c1; kgd_spec_fontsize=font-small; kgd_spec_img=imageson; navgoco=%7B%22nav%22%3A%7B%7D%7D',
                ];

            $path = sprintf("%s/web/images/captcha.png", \Yii::getAlias('@frontend'));

            $captcha =
                $parsesService->receiveNameCaptcha(
                    $parsesService->downloadCaptcha(
                        'http://kgd.gov.kz/apps/services/CaptchaWeb/generate?uid=a4f28ff3-64cd-4d73-be98-35bf69317970&t=d0940e65-2201-44a6-9e64-c408350fe58c',
                        $path
                    )
                );

            $data =
                sprintf(
                    '{"iinBin":"%s","captcha-user-value":"%s","captcha-id":"a4f28ff3-64cd-4d73-be98-35bf69317970"}',
                    trim($postRequest['iin']),
                    $captcha
                );

            $response =
                Request::post(
                    'http://kgd.gov.kz/apps/services/culs-taxarrear-search-web/rest/search', $headers,
                    $data
                );

            $person = json_decode($response->raw_body, true);
        }

        return $this->render('index', ['person' => $person]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $postRequest = \Yii::$app->request->post();

        if (!empty($postRequest)) {
            $model = new People(
                [
                    'iin_bin' => $postRequest['iinBin'],
                    'full_name' => $postRequest['nameRu'],
                    'total_arrears' => $postRequest['totalArrear'],
                    'pension_contribution_arrears' => $postRequest['pensionContributionArrear'],
                    'social_contribution_arrears' => $postRequest['socialContributionArrear'],
                    'social_healthInsurance_arrears' => $postRequest['socialHealthInsuranceArrear'],
                    'appealed_amount' => $postRequest['appealledAmount'],
                    'modified_terms_amount' => $postRequest['modifiedTermsAmount'],
                    'rehabilitation_procedure_amount' => $postRequest['rehabilitaionProcedureAmount'],
                    'send_time' => $postRequest['sendTime'],
                    'tax_org_info' => $postRequest['taxOrgInfo'],
                    'is_status' => true,
                ]
            );

           if ( $model->save(false)){
               \Yii::$app->session->setFlash('success', 'Data saved successfully');

               return $this->redirect('index');
           }
        }

        \Yii::$app->session->setFlash('error', 'There was an error sending your data.');

        return $this->redirect('index');
    }
}
