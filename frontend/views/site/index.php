<?php

/* @var $this yii\web\View */

$this->title = 'Smart Technologies';

$homeUrl = Yii::$app->homeUrl;

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Smart Technologies!</h1>

        <p><a class="btn btn-lg btn-success" href="<?= $homeUrl ?>parsers">Get started </a></p>
    </div>

</div>
