<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\People;

/**
 * PeopleControl represents the model behind the search form of `common\models\People`.
 */
class PeopleControl extends People
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_status'], 'integer'],
            [
                [
                    'iin_bin',
                    'full_name',
                    'total_arrears',
                    'pension_contribution_arrears',
                    'social_contribution_arrears',
                    'social_healthInsurance_arrears',
                    'appealed_amount',
                    'modified_terms_amount',
                    'rehabilitation_procedure_amount',
                    'send_time',
                    'tax_org_info',
                    'created_at',
                    'updated_at',
                ],
                'trim',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = People::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_status' => $this->is_status,
        ]);

        $query->andFilterWhere(['ilike', 'iin_bin', $this->iin_bin])
            ->andFilterWhere(['ilike', 'full_name', $this->full_name])
            ->andFilterWhere(['ilike', 'total_arrears', $this->total_arrears])
            ->andFilterWhere(['ilike', 'pension_contribution_arrears', $this->pension_contribution_arrears])
            ->andFilterWhere(['ilike', 'social_contribution_arrears', $this->social_contribution_arrears])
            ->andFilterWhere(['ilike', 'social_healthInsurance_arrears', $this->social_healthInsurance_arrears])
            ->andFilterWhere(['ilike', 'appealed_amount', $this->appealed_amount])
            ->andFilterWhere(['ilike', 'modified_terms_amount', $this->modified_terms_amount])
            ->andFilterWhere(['ilike', 'rehabilitation_procedure_amount', $this->rehabilitation_procedure_amount])
            ->andFilterWhere(['ilike', 'send_time', $this->send_time])
            ->andFilterWhere(['ilike', 'tax_org_info', $this->tax_org_info])
            ->andFilterWhere(['ilike', 'created_at', $this->created_at])
            ->andFilterWhere(['ilike', 'updated_at', $this->updated_at])
        ;

        return $dataProvider;
    }
}
