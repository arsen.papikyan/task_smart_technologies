<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PeopleControl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="people-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'iin_bin') ?>

    <?= $form->field($model, 'full_name') ?>

    <?= $form->field($model, 'total_arrears') ?>

    <?= $form->field($model, 'pension_contribution_arrears') ?>

    <?php // echo $form->field($model, 'social_contribution_arrears') ?>

    <?php // echo $form->field($model, 'social_healthInsurance_arrears') ?>

    <?php // echo $form->field($model, 'appealed_amount') ?>

    <?php // echo $form->field($model, 'modified_terms_amount') ?>

    <?php // echo $form->field($model, 'rehabilitation_procedure_amount') ?>

    <?php // echo $form->field($model, 'send_time') ?>

    <?php // echo $form->field($model, 'tax_org_info') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'is_status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
